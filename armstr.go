package main
import "fmt"

func main() {
	var number,tempNum,remainder int
	var result int =0
	fmt.Print("Enter any three digit number : ")
	fmt.Scan(&number)

	tempNum = number

	for {
		remainder = tempNum%10
		result += remainder*remainder*remainder		
		tempNum /=10
		
		if(tempNum==0){
			break 
		}
	}

	if(result==number){
		 fmt.Printf("%d is an Armstrong number.",number)
	}else{
		fmt.Printf("%d is not an Armstrong number.",number)
	}
}